// example object
example = {
    elements: {
        0:'elem0',
        1:'elem1',
        2:'elem2',
        3:'elem3',
        4:'elem4'
    },
    params: {
        'pepe': ['dos','cuatro'],
        'jorge': ['cinco','tres']
    }
};
example1 = {
    elements: {
        0:'water',
        1:'fire',
        2:'wind'
    },
    params: {
        'params1':['p11','p12','5665'],
        'params2':['p21','p22','p23']
    }
};

(function () {
    //    temp, get, set, save, load, clear
    var localService = function () {
        var local= this;
        
        /* clear all > param temp or saved */
        local.clearAll = function (env) {
            delete localStorage[env];
        };
        /* clear item > param item */
        local.clearItem = function (env, key) {
            var envVar = JSON.parse(localStorage[env]);
            delete envVar[key];
            localStorage[env] = JSON.stringify(envVar);
        };
        
        
        /* set all values clearing last env */
        local.setAll = function (env, elem) {
            var newVal = JSON.stringify(elem);
            localStorage[env] = newVal;
        };
        local.setItem = function (env, key, val) {
            var envVar = JSON.parse(localStorage[env]);
            envVar[key] = val;
            if ((val!=='') || (val === undefined)) {
                localStorage[env] = JSON.stringify(envVar);
            }
            else {
                delete envVar[key];
                localStorage[env] = JSON.stringify(envVar);
            }
        };
        
        
        /* get value */
        local.getAll = function (env) {
            return localStorage[env];
        };
        local.getItem = function (env, item) {
            var getObj = JSON.parse(localStorage[env]);
            return getObj[item];
        };
        
        /* save to saved obj */
        local.save = function (from, to) {
            localStorage[to] = localStorage[from];
        };
        /* load from saved obj to temp obj */
        local.load = function (from) {
            localStorage.temp = localStorage[from];
        };
        
    };
}());

//** Documentation **//

//Instantiate functionality (need run this first before all)
    //asd= new localService();

//Set All (params env and obj) Reeplace all of existing env
    //asd.setAll('temp',example);

/*Set Item value (params env, key and val)
    >> if val doesn't exists remove key
    >> if key doesn't exists create
    >> if key exists replace*/
//asd.setItem('temp','el',example1.params1);

//Get All from env
    //asd.getAll('temp');

//Get Item parameters env and item
    //asd.getItem('temp','params');
    
//Clear Item (params env and key)
    //asd.clearItem('temp','elements');